# Vue Scaffold

A project created with Vue, Vuetify, and Webpack. The puspose is to fork this project as a scaffold for any new projects requiring this structure, to follow a standard across all projects.

**Features**

* Formatting rules for Prettier, TS, and Vue
* Language and compilation rules for TS
* Folder structure and layout 
* Firebase integration
* Dev / Staging / Production building

## F5 Solutions (pty) Ltd