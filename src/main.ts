import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'

import VueInstall from '@/services/vueInstall'

Vue.config.productionTip = false

const vueWithServices = Vue.use(VueInstall)

const firebaseService = vueWithServices.prototype.$firebaseService

router.beforeEach(async (to, from, next) => {
  const loggedIn = await firebaseService.loggedIn()
  if (to.meta.secure) {
    if (loggedIn) {
      next()
    } else {
      next({ name: 'login' })
    }
  }

  next()
})

let vueApp
firebaseService.auth.onAuthStateChanged((t) => {
  store.commit('authorized', t !== null)
  if (!vueApp) {
    vueApp = new Vue({
      router,
      store,
      vuetify,
      render: h => h(App)
    }).$mount('#app')
  }
})
