import ApiService from './services/api/apiService'
import FirebaseService from './services/firebase/firebaseService'

declare module 'vue/types/vue' {
  interface Vue {
    $apiService: ApiService
    $firebaseService: FirebaseService
  }
}
