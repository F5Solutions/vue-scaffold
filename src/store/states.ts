import { BasicInterface } from '@/interfaces/BasicInterface'

export interface RootState {
  authorized: boolean
}

export interface BasicState {
  basicItem: BasicInterface
}