import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

import { basicModule } from './modules/basicModule'
import { RootState } from './states'

const vuexLocal = new VuexPersistence({
  key: 'f5s-vue-scaffold',
  storage: window.localStorage
  // modules: ['basicModule'] // Optionally set which module to save
  //reducer: (state: any) => state // Optionally reduce the state to save only necessary items
})

Vue.use(Vuex)

export default new Vuex.Store<RootState>({
  plugins: [vuexLocal.plugin],
  state: {
    authorized: false
  },
  mutations: {
    authorized(state, authorized) {
      state.authorized = authorized
    }
  },
  actions: {},
  getters: {
    authorized(state) {
      return state.authorized
    }
  },
  modules: {
    basicModule
  }
})
