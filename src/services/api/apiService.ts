import config from '@/database/config.json'

export default class ApiService {
  private baseUrl = config.apiUrl

  // MOCK of a post
  post(form: any): Promise<any> {
    return new Promise(res => {
      console.log(`POST to: ${this.baseUrl}`)
      setTimeout(() => {
        res()
      }, 1000)
    })
  }

  // MOCK of a get
  get(form: any): Promise<any> {
    return new Promise(res => {
      console.log(`GET to: ${this.baseUrl}`)
      setTimeout(() => {
        res()
      }, 1000)
    })
  }
}
