import config from '@/database/config.json'

import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/storage'
import 'firebase/auth'
import 'firebase/functions'

export default class FirebaseService {
  firebaseInstance: firebase.app.App

  firestore: firebase.firestore.Firestore
  storage: firebase.storage.Storage
  auth: firebase.auth.Auth

  constructor() {
    this.firebaseInstance = firebase.initializeApp(config.firebase)

    this.firestore = firebase.firestore()
    this.storage = firebase.storage()

    this.auth = firebase.auth()
  }

  // TODO: Switch the bottom lines when config set
  loggedIn() {
    return true
    // return this.auth.currentUser !== null
  }
}
