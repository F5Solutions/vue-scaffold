import FirebaseService from './firebase/firebaseService'
import ApiService from './api/apiService'


// This exports the plugin object.
export default {
  // The install method will be called with the Vue constructor as
  // the first argument, along with possible options
  install(Vue: any) {
    Vue.prototype.$firebaseService = new FirebaseService()
    Vue.prototype.$apiService = new ApiService()
  }
}
